# FitManager

## `FitManager` class

Constructor:
  * `FitManager(RooWorkspace* w = nullptr, const char* poi = "mu")` : Specify a workspace or leave blank to let manager try find workspace in current directory. If existing workspace has a `ModelConfig` in it this will be used, otherwise `poi` specified could be used and other parameters inferred.

Manager state inspection:
  * `RooAbsPdf* pdf()` : currently selected pdf
  * `RooArgSet& poi()` : parameters of interest, if any
  * `RooArgSet* globs()` : names of the global observables, if any
  * `std::pair<RooAbsData*, const RooArgSet*> data()` : currently selected dataset and corresponding global observable values
  * `const FitManager::FitConfig& fitConfig()` : current fit configuration
  
Manager state modification:
  * `setPdf(RooAbsPdf* pdf)` : change the pdf
  * `setData(RooAbsData* data, RooArgSet* globs)` : change data (returns previously set data pair)
  * `setPllModifier(modifier)` : Controls behaviour of the `pll` and `expected_pll` functions (see below). Choose `modifier` from:
    * `FitManager::TwoSided` : two sided test statistic ($t_\mu$) - default
    * `FitManager::OneSidedPositive` : one sided for limits ($q_\mu$)
    * `FitManager::OneSidedNegative` : one sided for discovery ($q_0$)
    * `FitManager::OneSidedAbsolute` : special case for setting limits on magnitude of poi
    * `FitManager::Uncapped` : two sided for discovery -- is negative for underfluctuations

Doing fits:
  * `RooFitResult* fit()` : runs/retrieves a fit on the current state of the workspace+manager.
  * `RooRealVar* first_poi()` : pointer to first parameter of interest. E.g. to do a conditional fit, set parameter constant and call `fit()` method.
  * `double pll()` : compute observed pll value (i.e. based on currently loaded data). Uses current pll modifier.
  * `double expected_pll(int nSigma=0)` : compute expected n-sigma pll value
  * `double sigma_mu(double asimovValue=0)` : estimate the sigma parameter using an asimov dataset with the given poi value
  
Generating data:
  * `std::pair<RooAbsData*, const RooArgSet*> generatedExpected()`: returns the asimov dataset (and global observables) after first performing a conditional fit (i.e. holding poi constant at current values) to the current data.
  
Looping over existing fits:
  * `fitResults().size()` : number of managed fits
  * `FitManager::FitResult* fitResults().at(size_t i)` : get i-th fit. See below for `FitResult` class details
  * `FitManager::FitResult* find(std::function<bool(const FitManager::FitResult&)>)` : calls the function on each fit result, stopping as soon as the function returns true. To run some code over all the fits just always return false. 
  
 Writing to disk:
  * `writeToFile(const char* fileName, const char* wsFilename = nullptr)` : write the workspace and all the fit results, fit configs, and generated data to the given file. If `wsFilename` is specified, workspace will be written to that file separately. 


## `FitResult` class

Has the following attributes and methods:
 * `fitResult` : the `RooFitResult*` object.
 * `dataName` : the name of the dataset.
 * `fitConfig_uuid` : the uuid of the fit configuration. 
 * `fitConfig()` : will try to locate the config in the manager.
