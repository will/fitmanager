//
// Created by Will Buttinger on 24/08/2020.
//

#include "FitManager/FitManager.h"

#include "RooStats/AsymptoticCalculator.h"

#include "Math/ProbFuncMathCore.h"
#include "RooMinimizer.h"
#include "RooDataSet.h"

#include "Math/GenAlgoOptions.h"

#include "Math/BrentRootFinder.h"
#include "Math/WrappedFunction.h"
#include "Math/ProbFunc.h"

#include "TSystem.h"
#include "TFile.h"
#include "TKey.h"

#include <string>
#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <memory>
#include <functional>
#include <limits>
#include <cmath>
#include <algorithm>
#include <map>

TObject* FitManager::m_msgObj = 0;


FitManager::FitManager(RooWorkspace* ws, const char* poi) : m_ws(ws) {


    if (ws == nullptr && gDirectory) {
        // try to find a workspace in the current directory
        auto keys = gDirectory->GetListOfKeys();
        if (keys) {
            for (auto &&k : *keys) {
                auto cl = TClass::GetClass(((TKey *) k)->GetClassName());
                if (cl == RooWorkspace::Class() || cl->InheritsFrom("RooWorkspace")) {
                    m_ws = dynamic_cast<RooWorkspace *>(gDirectory->Get(k->GetName()));
                    if (m_ws) break;
                }
            }
        }
    }

    if (!m_ws) {
        Info("FitManager::FitManager","Creating New Workspace");
        m_ws = new RooWorkspace("workspace","workspace");
        poi = nullptr;
    }

    m_fitConfigs.init("FitConfigs", this);
    m_generated.init("GeneratedData",this);
    m_fitResults.init("FitResults", this);

    if (m_fitConfigs.m_tree && m_fitConfigs.m_tree->GetEntriesFast() > 0) {
        // load the last config --- TODO: don't load it this lazy way (can jump straight to last config)
        auto count = m_fitConfigs.m_tree->GetEntriesFast();
        size_t i = 0;
        m_fitConfig = *(fitConfigs().find([&](const FitConfig&) { i++; return i==count; } ));
    } else {
        m_fitConfig.fitConfig.SetParabErrors(true); // will use to run hesse after fit
        m_fitConfig.fitConfig.MinimizerOptions().SetMinimizerType("Minuit2");
        m_fitConfig.fitConfig.MinimizerOptions().SetStrategy(0);
        m_fitConfig.fitConfig.MinimizerOptions().SetPrintLevel(-1);
        m_fitConfig.fitConfig.MinimizerOptions().SetExtraOptions(ROOT::Math::GenAlgoOptions());
        // have to const cast to set extra options
        const_cast<ROOT::Math::IOptions *>(m_fitConfig.fitConfig.MinimizerOptions().ExtraOptions())->SetValue(
                "StrategySequence", "012");

        // default nll construction options
        m_fitConfig.nllOpts.Add( RooFit::Offset(true).Clone() );
    }

    if (m_fitConfig.rootVersion != gROOT->GetVersion()) {
        m_fitConfig.rootVersion = gROOT->GetVersion();
        m_fitConfig.uuid = "";
    }

    // if there's a RooStats ModelConfig object present, use that to configure self
    RooStats::ModelConfig* mc = nullptr;
    for(auto o : m_ws->allGenericObjects()) {
        mc = dynamic_cast<RooStats::ModelConfig*>(o);
        if (mc) break;
    }

    if (mc) {
        Info("FitManager::FitManager","Configuring using ModelConfig: %s",mc->GetName());
        setPdf( mc->GetPdf() );
        Info("FitManager::FitManager","PDF: %s", mc->GetPdf()->GetName());
        // find first dataset that has all the observables in it
        auto dset = m_ws->allData();
        for(auto& d : dset) {
            if (d->get()->equals(*mc->GetObservables())) {
                Info("FitManager::FitManager","Data: %s", d->GetName());
                TString globsName = Form("%s_globs",d->GetName());
                if (mc->GetGlobalObservables()) {
                    m_fitConfig.nllOpts.Add(RooFit::GlobalObservables(*mc->GetGlobalObservables()).Clone());
                    if( m_ws->_snapshots.find(globsName) == nullptr) {
                        Warning("FitManager::FitManager", "Using current values for global observables");
                        m_ws->saveSnapshot(globsName, *mc->GetGlobalObservables());
                    }
                }
                setData(d, (mc->GetGlobalObservables()) ? m_ws->getSnapshot(globsName) : nullptr);
                break;
            }
        }
        if(!globs() && mc->GetGlobalObservables()) {
            m_fitConfig.nllOpts.Add(RooFit::GlobalObservables(*mc->GetGlobalObservables()).Clone());
        }
        Info("FitManager::FitManager","Global Observables: %s", (mc->GetGlobalObservables()) ? mc->GetGlobalObservables()->contentsString().c_str() : "<NONE>");

        if(mc->GetParametersOfInterest()) {
            setPOI(mc->GetParametersOfInterest()->contentsString().c_str());
        }
        Info("FitManager::FitManager", "POI: %s", (m_poi.empty() ? "" : m_poi.contentsString().c_str()));

        return;
    }


    // select pdf as first 'top level' pdf
    auto set = m_ws->allPdfs();
    auto itr = set.fwdIterator();
    RooAbsPdf* _pdf = nullptr;
    while( (_pdf = (RooAbsPdf*)itr.next())) {
        if (_pdf->hasClients()) continue; // only top-level pdfs
        Info("FitManager::FitManager","PDF: %s", _pdf->GetName());
        setPdf(_pdf);
        break;
    }

    // select data as first dataset
    auto dset = m_ws->allData();
    if (!dset.empty()) {
        auto _data = (*dset.begin());
        Info("FitManager::FitManager","Data: %s", _data->GetName());
        setData(_data);
    }

    // infer global observables at this stage, if we can
    // infer global observables and add to nllOpts
    if (pdf() && data().first) {
        auto gobs_and_np = std::shared_ptr<RooArgSet>(pdf()->getParameters(*data().first));
        auto obs = std::shared_ptr<RooArgSet>(pdf()->getObservables(*data().first));
        RooArgSet *_gobs = new RooArgSet("globalObservables");
        _gobs->add(*gobs_and_np); //will remove the np in a moment
        delete pdf()->getAllConstraints(*obs, *gobs_and_np);
        _gobs->remove(*gobs_and_np); //gobs_and_np now only contains the np
        if (_gobs->empty()) {
            Info("FitManager::FitManager","Inferred Global Observables: <NONE>");
        } else {
            Info("FitManager::FitManager", "Inferred Global Observables: %s", _gobs->contentsString().c_str());
        }
        m_fitConfig.nllOpts.Add(RooFit::GlobalObservables(*_gobs).Clone());
        setData(data().first, _gobs->snapshot());  // snapshot the global observable current values

        delete _gobs; // nllOpts owns list
    }

    if (poi) {
        setPOI(poi);
        Info("FitManager::FitManager", "POI: %s", (m_poi.empty() ? "" : m_poi.contentsString().c_str()));
    }

}

void FitManager::setPdf(RooAbsPdf* _pdf) {
    if (_pdf != m_pdf) {
        SafeDelete(m_nll); // invalidate current nll
    }
    m_pdf = _pdf;
    if(m_fitConfig.pdfName != m_pdf->GetName()) {
        m_fitConfig.pdfName = m_pdf->GetName();
        m_fitConfig.uuid = ""; // mark as a potentially 'new' config
    }
}

std::pair<RooAbsData*,const RooArgSet*> FitManager::setData(RooAbsData* _data, const RooArgSet* _gobs) {
    auto out = m_data;
    m_data.first = _data;
    SafeDelete(m_obs);
    if (pdf() && _data) {
        m_obs = pdf()->getObservables(_data);
    }
    if ( (_gobs && (!_gobs->empty()) && !(globs() && globs()->equals(*_gobs))) || (!_gobs && (globs() && !globs()->empty()) ) ) {
        Warning("setData","Global observables mismatch");
        if (_gobs) _gobs->Print();
        if (globs()) globs()->Print();
    } else if (_gobs && globs()) {
        m_data.second = _gobs;
        // globs may have come decoupled (after saving and reloading) from workspace so assign through
        *(std::shared_ptr<RooAbsCollection>(ws()->allVars().selectCommon(*globs())).get()) = *_gobs;
        //*globs() = *_gobs; // set the global observables to their correct values
    }
    if (m_nll) {
        m_nll->setAttribute("needUpdate",true); // postpone update to when actually need the nll
    }
    return out;
}

RooAbsReal* FitManager::nll() {
    if (m_nll) {
        if (m_nll->getAttribute("needUpdate")) {
            m_nll->setData(*data().first, false /* should we clone data? */);
            m_nll->setAttribute("needUpdate",false);
        }
        return m_nll;
    }
    auto _pdf = pdf();
    if (!_pdf) return nullptr;
    auto _data = data();
    if (!_data.first) return nullptr;
    // const optimization will bork the nll if anything is const that we will later make unconst
    // TODO: how to ensure this?
    // for now, ensure the poi are floating for the nll creation at least
    auto _snap = poi().snapshot();
    setAttribAll(poi(), "Constant", false);
    m_nll = _pdf->createNLL(*_data.first, m_fitConfig.nllOpts);
    poi() = *_snap;
    delete _snap;
    return m_nll;
}

void FitManager::setPOI(const char* poi) {
    m_poi.removeAll();
    m_poi.add( ws()->argSet(poi) );
}

RooArgSet* FitManager::obs() {
    return m_obs;
}

RooArgSet* FitManager::globs() {
    auto a = dynamic_cast<RooCmdArg*>( m_fitConfig.nllOpts.FindObject("GlobalObservables") );
    if (!a) return nullptr;
    return const_cast<RooArgSet*>(a->getSet(0));
}


std::pair<RooAbsData*, RooArgSet*> FitManager::generateExpected() {
    std::pair<RooAbsData*,RooArgSet*> out = std::make_pair(nullptr,nullptr);

    auto _pdf = pdf();
    if (!_pdf) return out;

    // should ensure a conditional fit is performed to the 'obs' data before generating
    AutoRestorer snap(ws()->allVars());
    FitManager::setAttribAll(m_poi, "Constant", kTRUE);
    auto _hes = m_fitConfig.fitConfig.ParabErrors();
    m_fitConfig.fitConfig.SetParabErrors(false);
    auto conditionalFit = fit() ;
    m_fitConfig.fitConfig.SetParabErrors(_hes);

    auto _obs = obs();
    auto _gobs = globs();

    // check if we already have a dataset matching current state ...
    TUUID cFitID(conditionalFit->GetName());
    auto dataSelector = [&](const GeneratedData& d) {
        if (d.fitResult_uuid != cFitID) return false;
        return true;
    };

    if (auto saved = generatedData().find(dataSelector); saved) {
        return std::make_pair(saved->data, saved->gobs);
    }

    if (m_locked) throw DataUnavailableException();


    //Get the Asimov Dataset!
    // Note - doesnt produce correct result if observable is shared between channels with diff bining
    // also note: if binning is coarse compared to e.g. a narrow gaussian signal shape
    // then signal bump will lie between the generated points -- will miss the bump!
    // will then get weird fluctuations in limit scans (up-down as sensitive/not sensitive)

    if (!_pdf->canBeExtended()) {
        // because roofit built in expected data generation is broken for non-extended, must use roostats
        out.first = RooStats::AsymptoticCalculator::GenerateAsimovData(*_pdf , *_obs );
    } else {
        out.first = _pdf->generate(*_obs,RooFit::ExpectedData(),RooFit::Extended()); //must specify we want extended ... because if it's a RooSimultaneous, RooSimSplitGenContext wont generate non-integer weights unless 'extended'
    }

    out.first->SetName( TUUID().AsString() );

    if(_gobs && !_gobs->empty()) {
        out.second = _gobs->snapshot();

        //get all floating parameters ...
        RooArgSet* allPars = _pdf->getParameters(_obs);
        RooAbsCollection* floatPars = allPars->selectByAttrib("Constant",kFALSE);
        RooArgSet temp; temp.add(*floatPars);/*use a copy since don't want to modify the np set*/
        RooArgSet* constraints = _pdf->getAllConstraints(*_obs,temp);

        //loop over constraint pdfs, recognise gaussian, poisson, lognormal
        TIterator* citer = constraints->createIterator();
        RooAbsPdf* pdf = 0;
        while((pdf=(RooAbsPdf*)citer->Next())) {
            //determine which gobs this pdf constrains. There should only be one!
            std::unique_ptr<RooArgSet> cgobs(pdf->getObservables(out.second));
            if(cgobs->getSize()!=1) { std::cout << "constraint " << pdf->GetName() << " constrains " << cgobs->getSize() << " global observables. skipping..." << std::endl; continue; }
            RooAbsArg* gobs_arg = cgobs->first();

            //now iterate over the servers ... the first server to depend on a nuisance parameter we assume is the correct server to evaluate ..
            std::unique_ptr<TIterator> itr(pdf->serverIterator());
            for(RooAbsArg* arg = static_cast<RooAbsArg*>(itr->Next()); arg != 0; arg = static_cast<RooAbsArg*>(itr->Next())) {
                RooAbsReal * rar = dynamic_cast<RooAbsReal *>(arg);
                if( rar && ( rar->dependsOn(*floatPars) ) ) {
                    out.second->setRealValue(gobs_arg->GetName(),rar->getVal()); //NOTE: We could add some checks that all gobs actually get set
                }
            }
        }
        delete citer;
        delete constraints;
        delete floatPars;
        delete allPars;
    }

    GeneratedData d;
    d.fitResult_uuid.SetFromString( conditionalFit->GetName() );
    d.gobs = out.second;
    d.data = out.first;
    m_generated.m_mem.emplace_back( new GeneratedData(d) );

    return out;
}

// if nSigma specified, will compute an expected nSigma asymptotic p-value
std::pair<double,double> FitManager::asymptoticPValue(RooFitResult** _unconditionalFit, double* nSigma, double** _sigma_mu, double alt_val) {

    auto out = std::make_pair( std::numeric_limits<double>::quiet_NaN() , std::numeric_limits<double>::quiet_NaN() );

    auto mu = first_poi();
    if (!mu) return out;

    double sigma = 0;
    if(_sigma_mu && *_sigma_mu) sigma = **_sigma_mu;
    if(sigma<=0) {
        sigma = sigma_mu(alt_val);
        if(_sigma_mu) *_sigma_mu = new double(sigma); //updates _sigma_mu
    }
    double* s = &sigma;
    double _pll = pll(_unconditionalFit, nSigma, &s, alt_val);
    if (std::isnan(_pll)) {
        return out;
    }
    out = std::make_pair(FitManager::nullPValue(_pll, mu, sigma, pllModifier()),
                         FitManager::altPValue(_pll, mu, alt_val, sigma, pllModifier()));
    if (out.second == 0 && out.first==0) {
        // protect against divisions by 0 by modifying
        out.second = 1e-20; out.first = 1e-20;
    }
    return out;

}

double FitManager::sigma_mu(double asimovValue) {
    RooRealVar* mu = first_poi();
    if (!mu) return -1;

    // is asimovValue equal to current mu value, wont be able to estimate sigma_mu
    if ( std::abs(mu->getVal() - asimovValue) < 1e-10 ) {
        //Error("sigma_mu","Cannot estimate sigma_mu for %f using asimovValue=%f",mu->getVal(), asimovValue);
        //return -1;

        //shift alt_val so that it's different to testing value ...
        if(mu->getVal() - 1. >= mu->getMin()) asimovValue -= 1.;
        else asimovValue += 1.; // how to do this well???
    }

    /// get the asimov dataset for the requested value
    double tmp = mu->getVal();
    mu->setVal(asimovValue);
    std::pair<RooAbsData*,RooArgSet*> asi;
    try {
        asi = generateExpected();
    } catch(UnavailableException& e) {
        mu->setVal(tmp); throw e;
    }
    mu->setVal(tmp);

    auto tmp_modifier = pllModifier();auto _data = data();
    setPllModifier(TwoSided);setData(asi.first, asi.second);
    double pll_asimov;
    try {
        pll_asimov = pll();
    } catch(UnavailableException& e) {
        setPllModifier(tmp_modifier);setData(_data.first, _data.second); // restore state
        throw e;
    }
    setPllModifier(tmp_modifier);setData(_data.first, _data.second); // restore state

    if(pll_asimov < 1e-15) pll_asimov = 1e-15; //protect against 0 and negative values
    return  (fabs(mu->getVal() - asimovValue) / sqrt(pll_asimov) );

}




void FitManager::setGlobs(const RooArgSet* _globs) {
    // Important note: "null" globs can produce a different NLL function to an empty globs set
    // This is because in createNLL if globs is specified it is used as the paramSet in the ConstraintSum
    // but if it's not set than the 'Constrain' parameters ( inferred from a call to getParameters(data,false) if not explicitly specified)
    // will be used.

    bool changed = false;
    if(auto old = dynamic_cast<RooCmdArg*>(m_fitConfig.nllOpts.FindObject("GlobalObservables")); old) {
        if (_globs && old->getSet(0)->equals(*_globs)){
            return; // already matching
        } else {
            m_fitConfig.nllOpts.Remove(old);
            delete old;
            changed = true;
        }
    }

    if (_globs) {
        m_fitConfig.nllOpts.Add( RooFit::GlobalObservables(*_globs).Clone() );
        changed = true;
    }

    if (changed) {
        m_fitConfig.uuid = "";
        SafeDelete(m_nll); m_nll = nullptr;
        if (pdf() && data().first) {
            std::string setName = Form("CACHE_CONSTR_OF_PDF_%s_FOR_OBS_%s", pdf()->GetName(),
                                       RooNameSet(*data().first->get()).content());
            if (ws()->_namedSets.count(setName)) ws()->removeSet(setName.c_str());
        }
    }


}


bool CollectionEquals(const RooAbsCollection& lhs, const RooAbsCollection& rhs) {

    if (lhs.getSize() != rhs.getSize()) return false;

    auto compareFunc = [](const RooAbsArg* left, const RooAbsArg* right) {
        if (left->namePtr() != right->namePtr()) return false;
        // TODO: what if not real valued?
        double l = dynamic_cast<const RooAbsReal *>(left)->getVal();
        double r = dynamic_cast<const RooAbsReal *>(right)->getVal();
        //if (std::abs(  l-r) > 1e-5) std::cout << left->GetName() << " " << l <<" vs " << r << std::endl;
        return std::abs(  l-r) <= 1e-9;//*(std::max(l,r));
    };
    return std::is_permutation(lhs._list.begin(), lhs._list.end(), rhs._list.begin(), compareFunc);
}


RooFitResult* FitManager::fit() {

    bool save = true;



    // ensure globs are at the correct values
    if (globs() && data().second) {
        // have to do it like this because the globs() returns the set from the nllOpts, if this
        // is an nllOpts loaded from the tree, the args are disconnected from the ws
        *(std::shared_ptr<RooAbsCollection>(ws()->allVars().selectCommon(*globs())).get()) = *data().second;
        //*globs() = *data().second;
    }

    std::unique_ptr<RooArgSet> allPars(pdf()->getParameters(data().first));
    std::unique_ptr<RooAbsCollection> floatPars(allPars->selectByAttrib("Constant",kFALSE));

    std::unique_ptr<RooAbsCollection> constPars( allPars->selectByAttrib("Constant",kTRUE) );

    // construct the fit-selection function. Requirements to select fit are:
    // - dataName must match current data
    // - fitResult.floatPars must 'equal' floatPars
    // - fitResult.constPars must equal and match values of const pars
    // - should also check the fitConfig in future (e.g. the pdfName etc)

    // should first determine if the current fitConfig state is compatible with any prior fitconfig
    // TODO: avoid such a check by always capturing changes to fit configs!

    auto fitSelect = [&](const FitResult& fr) {
        if (m_fitConfig.uuid != fr.fitConfig_uuid) return false; // TODO: make looser
        if (fr.dataName != data().first->GetName()) return false;
        if(!CollectionEquals(*constPars, fr.fitResult->constPars())) return false;

        // TODO: make next check unneceesary if check fitConfig etc?
        if (!fr.fitResult->floatParsFinal().equals(*floatPars)) return false; // only checks names match
        return true;
    };

    // check if a previously executed fit is consistent with the one being requested
    if (auto _fit = fitResults().find(fitSelect); _fit && _fit->fitResult) {
        // since this fit method puts float pars equal to best fit, must do that now too
        (*floatPars) = _fit->fitResult->floatParsFinal();
        return const_cast<RooFitResult*>(_fit->fitResult);
    }

    if (m_locked) {
        throw FitUnavailableException();
    }

    auto _nll = nll();
    if (!_nll) return nullptr;

    std::string s;
    if (m_fitConfig.fitConfig.MinimizerOptions().ExtraOptions() ) {
        m_fitConfig.fitConfig.MinimizerOptions().ExtraOptions()->GetNamedValue("StrategySequence", s);
    }
    TString m_strategy = s;

    int printLevel  =   m_fitConfig.fitConfig.MinimizerOptions().PrintLevel();
    int strategy =      m_fitConfig.fitConfig.MinimizerOptions().Strategy();
    //Note: AsymptoticCalculator enforces not less than 1 on tolerance - should we do so too?

    RooFit::MsgLevel msglevel = RooMsgService::instance().globalKillBelow();
    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(RooFit::FATAL);

    //check how many parameters we have ... if 0 parameters then we wont run a fit, we just evaluate nll and return ...
    if(floatPars->getSize()==0) {
        RooFitResult* result = 0;
        if(save) {
            //construct an empty fit result ...
            result = new RooFitResult(); // if put name here fitresult gets added to dir, we don't want that
            result->SetName(TUUID().AsString()); result->SetTitle("No fit");
            result->setFinalParList( RooArgList() );
            result->setInitParList( RooArgList() );
            result->setConstParList(  *allPars ); /* RooFitResult takes a snapshot of allPars */
            TMatrixDSym d;
            result->setCovarianceMatrix( d );
            result->setCovQual(-1);
            result->setMinNLL( _nll->getVal() );
            result->setEDM(0);
            result->setStatus(0);
            saveFit(*data().first, *result);
        }
        if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);
        return result;
    }


    RooMinimizer _minimizer(*_nll);
    _minimizer.fitter()->Config() = m_fitConfig.fitConfig;
    _minimizer.optimizeConst(2);
    //_minimizer.setPrintLevel(printLevel);
    //_minimizer.setMinimizerType(minim); // unneeded, gets set by minimize below
    //_minimizer.setStrategy(strategy);
    //_minimizer.setEps( tol );

    bool hesse = _minimizer.fitter()->Config().ParabErrors();
    _minimizer.fitter()->Config().SetParabErrors(false); // turn "off" so can run hesse as a separate step, appearing in status


    std::vector<TString> algNames;

    int status = 0;
    for(int tries=1,maxtries=4; tries <= maxtries; ++tries) {
        TString minim = _minimizer.fitter()->Config().MinimizerType();
        TString algo = _minimizer.fitter()->Config().MinimizerAlgoType();
        status = _minimizer.minimize(minim,algo);
        minim = _minimizer.fitter()->Config().MinimizerType(); // may have changed value
        if(save) algNames.push_back(_minimizer.fitter()->Config().MinimizerType()
                                    + _minimizer.fitter()->Config().MinimizerAlgoType() + std::to_string(_minimizer.fitter()->Config().MinimizerOptions().Strategy()));
        //int status = _minimizer->migrad();
        if(status%1000 == 0) break; //fit was good
        if(tries >= maxtries) break; //giving up

        //NOTE: minuit2 seems to distort the tolerance in a weird way, so that tol becomes 100 times smaller than specified
        //Also note that if fits are failing because of edm over max, it can be a good idea to activate the Offset option when building nll
        Warning("minimize","Fit Status=%d (edm=%f, tol=%f, strat=%d), Rescanning #%d...",status,_minimizer.fitter()->Result().Edm(),
                _minimizer.fitter()->Config().MinimizerOptions().Tolerance(),
                _minimizer.fitter()->Config().MinimizerOptions().Strategy(),tries);
        _minimizer.minimize(minim,"Scan");
        if(save) algNames.push_back(_minimizer.fitter()->Config().MinimizerType()+"Scan");
        if(tries == 2) { //up the strategy (if we can)
            int idx = m_strategy.Index('0'+strategy);
            if(idx!=-1 && idx!=m_strategy.Length()-1) {
                strategy = int(m_strategy(idx+1) - '0');
                _minimizer.setStrategy(strategy);
                tries--; //stick on tries=2 state
            } else {
                tries++; //move on to next
            }
        }
        if(tries == 3) { _minimizer.fitter()->Config().SetMinimizer("Minuit","migradImproved"); }
    }

    if(hesse) _minimizer.hesse(); //note: I have seen that you can get 'full covariance quality' without running hesse ... is that expected?

    RooFitResult* out = _minimizer.save("fitResult","fitResult");
    //before returning we will override _minLL with the actual NLL value ... offsetting could have messed up the value
    out->setMinNLL(_nll->getVal());

    // check if any of the parameters are at their limits (potentially a problem with fit)
    // or their errors go over their limits (just a warning)
    RooFIter itr = floatPars->fwdIterator();
    RooAbsArg* a = 0;
    int limit_status = 0;
    std::string listpars;
    while( (a = itr.next()) ) {
        RooRealVar* v = dynamic_cast<RooRealVar*>(a);
        if (!v) continue;
        double vRange = v->getMax() - v->getMin();
        if (v->getMin() > v->getVal()-vRange*0.0001 || v->getMax()< v->getVal()+vRange*0.0001) {
            // within 0.01% of edge

            // check if nll actually lower 'at' the boundary, if it is, refine the best fit to the limit value
            auto tmp = v->getVal();
            v->setVal( v->getMin() );
            double boundary_nll = _nll->getVal();
            if (boundary_nll <= out->minNll()) {
                ((RooRealVar*)out->_finalPars->find(v->GetName()))->setVal(v->getMin());
                out->setMinNLL(boundary_nll);
                Info("fit","Corrected %s onto minimum @ %g",v->GetName(),v->getMin());
            } else {
                // not better, so restore value
                v->setVal( tmp );
            }

            // if has a 'physical' range specified, don't warn if near the limit
            if (v->hasRange("physical"))
                limit_status = 900;
            listpars += v->GetName(); listpars += ",";
        } else if(hesse && (v->getMin() > v->getVal()-v->getError() || v->getMax() < v->getVal()
                                                                                     +v->getError())) {
            Info("minimize","PARLIM: %s (%f +/- %f) range (%f - %f)",v->GetName(),v->getVal()
                    ,v->getError(),v->getMin(),v->getMax());
            limit_status = 9000;
        }
    }
    if (limit_status == 900) {
        Warning("miminize","PARLIM: Parameters within 0.001%% limit in fit result: %s",
                listpars.c_str());
    } else if(limit_status > 0) {
        Warning("miminize","PARLIM: Parameters near limit in fit result");
    }



    if (save) {
        //modify the statusHistory to use the algnames instead ..
        int i=0; for(auto& s : algNames) { out->_statusHistory[i++].first=s; }
        // store the limit check result
        out->_statusHistory.push_back(std::make_pair("PARLIM", limit_status ));
        out->_status += limit_status;

    }

    if(status%1000 != 0) {
        Warning("minimize"," fit status %d",status);
    }

    if(printLevel < 0) RooMsgService::instance().setGlobalKillBelow(msglevel);

    // save the nllOpts, fit configuration and result to the store

    // minimizer may have slightly altered the fitConfig (e.g. unavailable minimizer etc) so update for that ...
    if (m_fitConfig.uuid.empty()) {
        m_fitConfig.fitConfig.MinimizerOptions() = _minimizer.fitter()->Config().MinimizerOptions();
    }

    out->SetName( TUUID().AsString() );



    saveFit(*m_data.first, *out);


    return out;
}


void FitManager::saveFit(const RooAbsData& data, const RooFitResult& result) {

    std::stringstream s;
    RooFIter itr = result.constPars().fwdIterator();
    RooAbsArg* arg = 0;
    while( (arg = itr.next()) ) {
        if (auto a = dynamic_cast<RooAbsReal*>(arg); a) {
            s << a->GetName() << "=" << a->getVal() << ";";
        }
    }
    Info("saveFit", "minNll=%f data = %s, constPars = %s", result.minNll(), data.GetName(), s.str().c_str() );

    if (m_fitConfig.uuid.empty()) {
        auto f = fitConfigs().find([&](const FitConfig& fc) {
            if (fc.pdfName != m_fitConfig.pdfName) return false;
            // TODO: check nllOpts, fitConfig
            if (fc.nllOpts.GetSize() != m_fitConfig.nllOpts.GetSize()) return false;
            auto itr = fc.nllOpts.fwdIterator();
            RooCmdArg* a = nullptr;
            while( (a = dynamic_cast<RooCmdArg*>(itr.next())) ) {
                auto o = m_fitConfig.nllOpts.FindObject(a->GetName());
                if (!o) return false;
                // TODO: Compare option content
            }
            return true;
        });
        if (f) {
            m_fitConfig.uuid = f->uuid;
        } else {
            // generate a new config id and add to our list of configs
            m_fitConfig.uuid = TUUID().AsString();
            //m_fitConfigs.emplace_back(m_fitConfig);
            m_fitConfigs.m_mem.emplace_back(new FitConfig(m_fitConfig) );
        }
    }
    m_fitResults.m_mem.emplace_back(new FitResult{.dataName=data.GetName(),.fitResult=&result,.fitConfig_uuid=m_fitConfig.uuid,.helper=this/*(RooFitResult*)result.Clone(result.GetName())*/});
}

void FitManager::writeToFile(const char* fileName, const char* wsFileName, bool newOnly) {

    // push new generated data into the workspace ...
    for(auto& d : m_generated.m_mem) {
        if (d->data && !ws()->data(d->data->GetName())) {
            ws()->import(*d->data);
            // because we will drop d.data (not stored properly in a tree?)
            // set the dataName attribute
            d->dataName = d->data->GetName();
        }
    }

    if (wsFileName == nullptr) wsFileName = fileName;

    // can use a blank filename to avoid writing the workspace entirely
    if (strlen(wsFileName)) { ws()->writeToFile(wsFileName); }

    TFile f1(fileName,(wsFileName==fileName) ? "UPDATE" : "RECREATE");
    m_fitConfigs.save(newOnly);
    m_fitResults.save(newOnly);
    m_generated.save(newOnly);
    f1.Close();

}

void FitManager::setAttribAll(const RooAbsCollection& coll, const char* name, Bool_t value) {
    RooFIter itr = coll.fwdIterator();
    RooAbsArg* arg = 0;
    while( (arg = itr.next()) ) {
        arg->setAttribute(name,value);
        arg->setValueDirty(); arg->setShapeDirty();
    }
}


double FitManager::pll(RooFitResult** _unconditionalFit, double* nSigma, double** _sigma_mu, double alt_val) {

    if (nSigma) {
        auto mu = first_poi();
        if (!mu) return std::numeric_limits<double>::quiet_NaN();

        double sigma = 0;
        if(_sigma_mu && *_sigma_mu) sigma = **_sigma_mu;
        if(sigma<=0) {
            sigma = sigma_mu(alt_val);
            if(_sigma_mu) *_sigma_mu = new double(sigma); //updates _sigma_mu
        }

        // determine the pll value corresponding to nSigma expected - i.e. where the altPValue equals e.g. 50% for nSigma=0,
        //find the solution (wrt x) of: FitManager::altPValue(x, var(poi), alt_val, _sigma_mu, _compatibilityFunction) - targetPValue = 0
        double targetTailIntegral = ROOT::Math::normal_cdf(*nSigma);

        // check how much of the alt distribution density is in the delta function @ 0
        // if more than 1 - target is in there, if so then pll must be 0
        double prob_in_delta = FitManager::Phi_m(mu->getVal(), alt_val, std::numeric_limits<double>::infinity(), sigma, pllModifier());
        // also get a contribution to the delta function for mu_hat < mu_L IF mu==mu_L
        if (mu->getVal() == mu->getMin("physical")) {
            // since mu_hat is gaussian distributed about mu_prime with std-dev = sigma
            // the integral is Phi( mu_L - mu_prime / (sigma) )
            double mu_L = mu->getMin("physical");
            prob_in_delta += ROOT::Math::normal_cdf((mu_L - alt_val) / sigma);
        }

        if (prob_in_delta > 1-targetTailIntegral) {
            return 0;
        }

        struct TailIntegralFunction {
            TailIntegralFunction(RooRealVar *_poi, double _alt_val, double _sigma_mu,
                                 PLLModifier _compatibilityFunction, double _target) :
                    poi(_poi), alt_val(_alt_val), sigma_mu(_sigma_mu), target(_target),
                    cFunc(_compatibilityFunction) {}

            double operator()(double x) const {
                return FitManager::altPValue(x, poi, alt_val, sigma_mu, cFunc) - target;
            }

            RooRealVar *poi;
            double alt_val, sigma_mu, target;
            PLLModifier cFunc;
        };

        TailIntegralFunction f(mu, alt_val, sigma, pllModifier(), targetTailIntegral);
        ROOT::Math::BrentRootFinder brf;
        ROOT::Math::WrappedFunction<TailIntegralFunction> wf(f);

        auto tmpLvl = gErrorIgnoreLevel;
        gErrorIgnoreLevel = kFatal;
        double _pll = 500.;
        double currVal(1.);
        int tryCount(0);
        double _prev_pll = _pll;
        do {
            currVal = wf(_pll);
            if (currVal > 1e-4) _pll = 2. * (_pll + 1.); // goto bigger pll scale
            else if (currVal < -1e-4) _pll /= 2.; // goto smaller pll scale
            //std::cout << "pll = " << _pll << " currVal = " << currVal << std::endl;
            brf.SetFunction(wf, 0, _pll);
            if (brf.Solve()) {
                _prev_pll = _pll;
                _pll = brf.Root();
            }
            //std::cout << " -- " << brf.Root() << " " << FitManager::altPValue(_pll, mu, alt_val, sigma, pllModifier()) << " >> " << wf(_pll) << std::endl;
            tryCount++;
            if (tryCount > 20) {
                gErrorIgnoreLevel = tmpLvl;
                Warning("FitManager::pll", "Reached limit nSigma=%g pll=%g", *nSigma, _pll);
                break;
            }
        } while (std::abs(wf(_pll)) > 1e-4 && std::abs(wf(_pll)) < std::abs(wf(_prev_pll)) * 0.99);
        gErrorIgnoreLevel = tmpLvl;
        //_pll *= 0.999; //subtract a little to capture delta function effects - only needed when doing toys
        //std::cout << "mu=" << mu->getVal() << " sigma=" << *nSigma << " pll = " << _pll << std::endl;
        return _pll;
    }



    auto _compatibilityFunction = pllModifier();

    //do an unconditional fit ...
    AutoRestorer snap( ws()->allVars() );
    FitManager::setAttribAll(poi(), "Constant", kFALSE);

    RooFitResult* unconditionalFit = 0;
    //std::shared_ptr<RooFitResult> fitPtr; // exists to auto-delete fit if needed

    if(_unconditionalFit && *_unconditionalFit) {
        unconditionalFit = *_unconditionalFit;
    } else {
        auto _hes = m_fitConfig.fitConfig.ParabErrors();m_fitConfig.fitConfig.SetParabErrors(false); //don't need hesse errors run
        unconditionalFit = fit();
        m_fitConfig.fitConfig.SetParabErrors(_hes);
        if (_unconditionalFit) *_unconditionalFit = unconditionalFit;
        //else fitPtr.reset( unconditionalFit ); // will ensure is deleted when exit
    }

    //put float parameters to unconditionalFit values ...
    ws()->allVars() = unconditionalFit->floatParsFinal();

    //put the just the poi back to their values (in prep for the conditional fit) ...
    m_poi = *snap.values();

    double sf = 1.;

    if(_compatibilityFunction(  ((RooAbsReal*)m_poi.first())->getVal() , ((RooAbsReal*)unconditionalFit->floatParsFinal().find(m_poi.first()->GetName()))->getVal() ) == 1) {
        if(_compatibilityFunction == FitManager::Uncapped) {
            //when doing the uncapped test statistic, we will flip the pll value when mu_hat is "compatible" with mu
            sf = -1.;
        } else {
            //best fit poi value is considered compatible with test value
            return 0;
        }
    }


    //do a conditional fit ...
    FitManager::setAttribAll(m_poi, "Constant", kTRUE);
    auto _hes = m_fitConfig.fitConfig.ParabErrors();m_fitConfig.fitConfig.SetParabErrors(false); //don't need hesse errors run
    auto conditionalFit = fit(); //std::shared_ptr<RooFitResult>( fit() );
    m_fitConfig.fitConfig.SetParabErrors(_hes);

    // if conditional fit "beat" unconditional fit, use the 'edm' as the deltaNLL
    if (conditionalFit->minNll() < unconditionalFit->minNll() - unconditionalFit->edm()) {
        Warning("pll","Conditional fit nll = %f < Unconditional nll = %f", conditionalFit->minNll(), unconditionalFit->minNll());
        return sf*2.0*(std::max( conditionalFit->edm(), unconditionalFit->edm()) );
    }

    // add and subtract edm's to be "pessimistic"

    return sf*2.0*(conditionalFit->minNll()+conditionalFit->edm() - (unconditionalFit->minNll() - unconditionalFit->edm()));
}




/* ASYMPTOTIC FORMULAE */




Double_t FitManager::Phi_m(double mu, double mu_prime, double a, double sigma, PLLModifier compatFunc ) {
    //implemented only for special cases of compatibility function
    if(compatFunc==TwoSided) {
        return 0;
    }

    if(compatFunc==OneSidedPositive) {
        //ignore region below x*sigma+mu_prime = mu ... x = (mu - mu_prime)/sigma
        if(a < (mu-mu_prime)/sigma) return 0;
        return ROOT::Math::gaussian_cdf(a) - ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
    } else if(compatFunc==OneSidedNegative) {
        //ignore region above x*sigma+mu_prime = mu ...
        if(a > (mu-mu_prime)/sigma) return ROOT::Math::gaussian_cdf((mu-mu_prime)/sigma);
        return ROOT::Math::gaussian_cdf(a);
    } else if(compatFunc==OneSidedAbsolute) {
        //cutoff at x*sigma+mu_prime = mu for mu<0 , and turns back on at x*sigma+mu_prime=mu for mu>0
        //ignore region between x = (-mu-mu_prime)/sigma and x = (mu-mu_prime)/sigma
        double edge1 = (-mu - mu_prime)/sigma;
        double edge2 = (mu-mu_prime)/sigma;

        if(edge1>edge2) { double tmp=edge1; edge1=edge2; edge2=tmp; }

        if(a<edge1) return ROOT::Math::gaussian_cdf(a);
        if(a<edge2) return ROOT::Math::gaussian_cdf(edge1);
        return ROOT::Math::gaussian_cdf(a) - (ROOT::Math::gaussian_cdf(edge2) - ROOT::Math::gaussian_cdf(edge1));
    }
    msg().Error("Phi_m","Unknown compatibility function ... can't evaluate");
    return 0;
}


//must use asimov dataset corresponding to mu_prime, evaluating pll at mu
Double_t FitManager::PValue(double k, double poiVal, double poi_primeVal, double sigma, double lowBound, double upBound, PLLModifier compatFunc) {
    //uncapped test statistic is equal to onesidednegative when k is positive, and equal to 1.0 - difference between twosided and onesidednegative when k is negative ...
    if(compatFunc==Uncapped) {
        //if(k==0) return 0.5;
        if(k>0) return PValue(k,poiVal,poi_primeVal,sigma,lowBound,upBound,OneSidedNegative);
        return 1. - (PValue(-k,poiVal,poi_primeVal,sigma,lowBound,upBound,TwoSided) - PValue(-k,poiVal,poi_primeVal,sigma,lowBound,upBound,OneSidedNegative));
    }


    //if(k<0) return 1.;
    if(k<=0) {
        if(compatFunc==OneSidedNegative && fabs(poiVal-poi_primeVal)<1e-9) return 0.5; //when doing discovery (one-sided negative) use a 0.5 pValue
        return 1.; //case to catch the delta function that ends up at exactly 0 for the one-sided tests
    }

    if(sigma==0 && (lowBound!=-std::numeric_limits<double>::infinity() || upBound != std::numeric_limits<double>::infinity())) {
        msg().Error("PValue","You must specify a sigma_mu parameter when parameter of interest is bounded");
        return -1;
    }

    //get the poi value that defines the test statistic, and the poi_prime hypothesis we are testing
    //when setting limits, these are often the same value

    Double_t Lambda_y = 0;
    if(fabs(poiVal-poi_primeVal)>1e-9) Lambda_y = (poiVal-poi_primeVal)/sigma;

    Double_t k_low = (lowBound == -std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((poiVal - lowBound)/sigma,2);
    Double_t k_high = (upBound == std::numeric_limits<double>::infinity()) ? std::numeric_limits<double>::infinity() : pow((upBound - poiVal)/sigma,2);

    double out = Phi_m(poiVal,poi_primeVal,std::numeric_limits<double>::infinity(),sigma,compatFunc) - 1;

    //go through the 4 'regions' ... only two of which will apply
    if( k <= k_high ) {
        out += ROOT::Math::gaussian_cdf(sqrt(k)+Lambda_y) - Phi_m(poiVal,poi_primeVal,Lambda_y + sqrt(k),sigma,compatFunc);
    } else {
        double Lambda_high = (poiVal - upBound)*(poiVal + upBound - 2.*poi_primeVal)/(sigma*sigma);
        double sigma_high = 2.*(upBound-poiVal)/sigma;
        out +=  ROOT::Math::gaussian_cdf((k-Lambda_high)/sigma_high) - Phi_m(poiVal,poi_primeVal,(k - Lambda_high)/sigma_high,sigma,compatFunc);
    }

    if( k <= k_low ) {
        out += ROOT::Math::gaussian_cdf(sqrt(k)-Lambda_y) + Phi_m(poiVal,poi_primeVal,Lambda_y - sqrt(k),sigma,compatFunc);
    } else {
        double Lambda_low = (poiVal - lowBound)*(poiVal + lowBound - 2.*poi_primeVal)/(sigma*sigma);
        double sigma_low = 2.*(poiVal - lowBound)/sigma;
        out +=  ROOT::Math::gaussian_cdf((k-Lambda_low)/sigma_low) + Phi_m(poiVal,poi_primeVal,(Lambda_low - k)/sigma_low,sigma,compatFunc);

        /*out +=  ROOT::Math::gaussian_cdf((k-Lambda_low)/sigma_low) +
                2*Phi_m(poiVal,poi_primeVal,(Lambda_low - k_low)==0 ? 0 : ((Lambda_low - k_low)/sigma_low),sigma,compatFunc)
                - Phi_m(poiVal,poi_primeVal,(Lambda_low - k)/sigma_low,sigma,compatFunc);
*/

        // handle case where poiVal = lowBound (e.g. testing mu=0 when lower bound is mu=0).
        // sigma_low will be 0 and gaussian_cdf will end up being 1, but we need it to converge instead
        // to 0.5 so that pValue(k=0) converges to 1 rather than 0.5.
        // handle this by 'adding' back in the lower bound
        // TODO: Think more about this?
        /*if (sigma_low == 0) {
            out -= 0.5;
        }*/
    }

    return 1. - out;
}

double FitManager::SearchAsymptoticPValue(FitManager& s, double low, double high, bool useCLs, double* nSigma, double alt_val, double alpha) {
    AutoRestorer snap(s.ws()->allVars());

    struct PValueFunction {
        PValueFunction(FitManager& _s, double* _nSigma, double _alt_val, double _target, bool _doCLs) :
                s(_s),
                alt_val(_alt_val), nSigma(_nSigma), target(_target), doCLs(_doCLs) {}

        double operator()(double x) const {
            s.first_poi()->setVal(x);
            auto res = s.asymptoticPValue(&uf,nSigma,nullptr,alt_val);
            if (doCLs) {
                return ((res.first==res.second) ? 1. : (res.first / res.second)) - target;
            }
            return res.first - target;
        }

        FitManager& s;
        double alt_val, target;
        double* nSigma = nullptr;
        bool doCLs;
        mutable RooFitResult* uf = nullptr;
    };

    PValueFunction f(s, nSigma, alt_val, alpha, useCLs);
    /*ROOT::Math::WrappedFunction<PValueFunction> wf(f);
    ROOT::Math::BrentRootFinder brf;
    brf.SetNpx(3);
    brf.SetFunction( wf, low, high );
     */

    // trying to find 0, for a function that is usually > 0 near min, and < 0 near max
    // do by zooming min and max in until difference is less than X% of the full range
    double max = high; double min = low;
    double rel_tolerance = 0.001;
    double abs_tol = 0.01;
    size_t max_eval = -1;

    double stop_diff = (max-min)*rel_tolerance;
    if (abs_tol > 0) {
        stop_diff = abs_tol;
    }

    double max_val = f(max);
    double min_val = f(min);

    if (max_val*min_val > 0) {
        std::cout << "BAD STRADDLE: " << min_val << " - " << max_val << std::endl;
        return std::numeric_limits<double>::quiet_NaN();
    }

    int step_count = 2;
    while( (max - min) > std::min((max + min)*rel_tolerance/2.,abs_tol) && step_count < max_eval ) {

        // evaluate the midpoint:
        double next = (max + min)/2.;
        double next_val = f( next );
        if (next_val == 0) return next;
        if (next_val*min_val > 0) {
            // on the same side of min_val;
            min = next;
            min_val = next_val;
        } else {
            max = next;
            max_val = next_val;
        }
        step_count++;
    }

    // interpolate between max and min to infer limit
    double y_delta = min_val - max_val;
    return min + (max - min)*(min_val)/y_delta;
}

#include <sys/wait.h>
#include <unistd.h>

void FitManager::ScanAndCalculate(double low, double high, double spacing, std::function<void(FitManager&)> calc, int nprocs) {
    if (high < low) return;
    AutoRestorer snap(ws()->allVars()); // will restore values when leave function

    double proc_spacing = (high-low)/(nprocs);

    std::vector<int> pids;
    for(size_t i_proc = 0; i_proc < nprocs; i_proc++) {

        // fork here
        int pid = -1;
        bool do_processing = (i_proc == nprocs-1) || ( (pid = fork()) == 0 );
        if (do_processing) {
            // determine starting point
            int j = 0;
            while( j*spacing < i_proc*proc_spacing ) j++;
            for(; (j*spacing < (i_proc+1)*proc_spacing) || (low+j*spacing == high); j++) {
                first_poi()->setVal(low+j*spacing );
                calc(*this);
            }
            // in a fork if pid is 0
            if (pid==0) {
                writeToFile(Form("results_%d.root",getpid()), "", true);
                exit(0);
            } else {
                if (nprocs > 1) {
                    Info("ScanAndCalculate","Waiting for %d procs to finish", nprocs-1);
                    int status;
                    while (wait(&status) > 0) {
                        std::cout << "Status: " << status << std::endl;
                    }
                    // load the results
                    for(auto& p : pids) {
                        TString fileName = Form("results_%d.root",p);
                        TFile f(fileName);
                        m_fitConfigs.load(&f);
                        m_fitResults.load(&f);
                        m_generated.load(&f);
                        gSystem->Unlink(fileName.Data());
                    }
                }
            }
        } else {
            pids.push_back(pid);
        }


    }


}


void FitManager::ScanAsymptoticPValue(FitManager& s, double low, double high, double spacing, std::vector<double>&& sigmas, double alt_val) {

    AutoRestorer snap(s.ws()->allVars()); // will restore values when leave function

    s.first_poi()->setVal(low);
    while( s.first_poi()->getVal() < high ) {
        if (sigmas.empty()) {
            // do observed p-value test statistic
            auto obs_pval = s.asymptoticPValue(nullptr, nullptr, nullptr, alt_val);
            // could print results here?
        } else {
            // do expected p-value
            for(auto& nSigma : sigmas) {
                auto exp_pval = s.asymptoticPValue(nullptr, &nSigma, nullptr, alt_val);
            }
        }
        s.first_poi()->setVal(s.first_poi()->getVal() + spacing);
    }

}

#include "TGraphAsymmErrors.h"

std::pair<double,double> FitManager::GetLimit(FitManager& s, bool useCLs, double* nSigma, double alt_val, double target) {


    s.setReadOnly(true);

    std::set<double> poiValues = s.conditionalValues();

    RooFitResult* uf = nullptr;
    std::pair<double,double> pvals;
    std::pair<double,double> mus;
    for(auto& x : poiValues) {
        s.first_poi()->setVal(x);
        try {
            auto pval = s.asymptoticPValue(&uf, nSigma, nullptr, alt_val);
            auto v = (useCLs) ? (pval.first / pval.second) : pval.first;
            if (v > target) {
                pvals.first = v;
                mus.first = x;
            } else {
                pvals.second = v;
                mus.second = x;
                break; // stop looping here
            }
        } catch(UnavailableException& e) {
            continue; // go to next available point
        }
    }

    s.setReadOnly(false);

    // interpolate between max and min to infer limit
    double y_delta = pvals.first - pvals.second;
    return std::make_pair(
            mus.first + (mus.second - mus.first)*(pvals.first - target)/y_delta,
            (mus.second - mus.first)/2.);


}

std::set<double> FitManager::conditionalValues() {
    std::set<double> poiValues;
    if (!first_poi()) return poiValues;
    fitResults().find( [&](const FitResult& fr) {
        //if (fr.dataName != s.data().first->GetName()) return false;
        if (auto _poi = dynamic_cast<RooRealVar*>(fr.fitResult->constPars().find(first_poi()->GetName())); _poi) {
            poiValues.insert(_poi->getVal());
        }
        return false; //loop on all fits
    } );
    return poiValues;
}

#include "TH1D.h"
#include "TMultiGraph.h"

TCanvas* FitManager::DrawPLL(FitManager& s) {
    AutoRestorer snap(s.ws()->allVars());
    s.setReadOnly(true);
    auto poiValues = s.conditionalValues();

    TGraph gPll; gPll.SetLineWidth(2); gPll.SetMarkerStyle(20); gPll.SetMarkerColor(kRed);

    std::map<int, TGraphAsymmErrors> gPll_expected;
    gPll_expected[0].SetLineStyle(2);
    gPll_expected[1].SetFillColor(kGreen);
    gPll_expected[2].SetFillColor(kYellow);

    RooFitResult* uf = nullptr;
    for(auto& x : poiValues) {
        double* sigma_mu = nullptr;
        s.first_poi()->setVal(x);
        try {
            gPll.SetPoint( gPll.GetN(), x, s.pll(&uf) );
        } catch(UnavailableException& e) {
        }
        for(int i=0; i<=2; i = -i + (i<=0)) {
            double ii = i;
            try {
                double _pll = s.pll(&uf, &ii, &sigma_mu, 0);
                if (i == 0) {
                    for (int j = 0; j < 3; j++) {
                        gPll_expected[j].SetPoint(gPll_expected[j].GetN(), x, _pll);
                    }
                } else {
                    auto &g = gPll_expected[abs(i)];
                    double diff = _pll - g.GetY()[g.GetN() - 1];
                    if (i >= 0) { g.SetPointEYhigh(g.GetN() - 1, diff); }
                    else { g.SetPointEYlow(g.GetN() - 1, -diff); }
                }
            } catch (UnavailableException &e) {
            }
        }
        if (sigma_mu) {delete sigma_mu;}
    }

    s.setReadOnly(false);

    TCanvas* c = new TCanvas("pllPlot","PLL Plot");
    TMultiGraph* mg = new TMultiGraph;

    if (gPll_expected[2].GetN()) {
        mg->Add ( (TGraph*)gPll_expected[2].Clone("expected2"), "AE3");
    }
    if (gPll_expected[1].GetN()) {
        mg->Add( (TGraph*)gPll_expected[1].Clone("expected1"),TString("E3") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }

    if (gPll_expected[0].GetN()) {
        mg->Add( (TGraph*)gPll_expected[0].Clone("expected"),TString("L") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }
    if (gPll.GetN()) {
        mg->Add( (TGraph*)gPll.Clone("observed"),TString("LP") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }

    mg->SetTitle(Form(";%s;%s",s.first_poi()->GetTitle(), s.pllModifier() == FitManager::TwoSided ? "#tilde{t}_{#mu}" : "#tilde{q}_{#mu}"));
    mg->Draw("A E3");

    if(gPad) gPad->RedrawAxis();


    return c;

}


TCanvas* FitManager::DrawAsymptoticPValue(FitManager& s, bool useCLs, double alt_val) {

    s.setReadOnly(true);

    // will produce a p-value plot of whatever is
    // computable without running more generates or fits

    // first determine poi values that we appear to have conditional fits for
    auto poiValues = s.conditionalValues();

    std::cout << "Found " << poiValues.size() << " possible scan points for " << s.first_poi()->GetName() << std::endl;

    TGraph gObs; gObs.SetTitle(Form("Observed %s", (useCLs) ? "CLs" : "CLs+b"));
    gObs.SetLineWidth(2); gObs.SetMarkerStyle(20); gObs.SetLineStyle(3); gObs.SetMarkerColor((useCLs) ? kRed : kBlue);

    TGraph gObs_null; gObs_null.SetTitle(Form("Observed %s", (useCLs) ? "CLs+b" : "CLs"));
    gObs_null.SetMarkerStyle(20);gObs_null.SetMarkerColor((useCLs) ? kBlue : kRed );
    TGraph gObs_alt; gObs_alt.SetTitle("Observed CLb");
    gObs_alt.SetMarkerStyle(20);

    std::map<int, TGraphAsymmErrors> gExp;
    gExp[0].SetLineStyle(2);
    gExp[1].SetFillColor(kGreen);
    gExp[2].SetFillColor(kYellow);

    RooFitResult* uf = nullptr;
    for(auto& x : poiValues) {
        s.first_poi()->setVal(x);
        double* sigma_mu = nullptr; // to reuse calculation of sigma_mu parameter at each test point

        try {
            auto obs_pval = s.asymptoticPValue(&uf, nullptr, &sigma_mu, alt_val);
            gObs.SetPoint(gObs.GetN(), x, obs_pval.first / (obs_pval.second));
            gObs_null.SetPoint(gObs_null.GetN(), x, obs_pval.first);
            gObs_alt.SetPoint(gObs_alt.GetN(), x, obs_pval.second);
        } catch(UnavailableException& e) {
            std::cout << "Skipping observed point @ mu = " << x << std::endl;
            // skip this point because fit unavailable
        }

        for(int i=0; i<=2; i = -i + (i<=0)) {
            double ii = i;
            try {
                auto exp_pval = s.asymptoticPValue(&uf, &ii, &sigma_mu, alt_val);
                if (i == 0) {
                    for (int j = 0; j < 3; j++) {
                        gExp[j].SetPoint(gExp[j].GetN(), x, (exp_pval.first) / (!useCLs ? 1. : exp_pval.second));
                    }
                } else {
                    auto &g = gExp[abs(i)];
                    double diff = (exp_pval.first / (!useCLs ? 1. : exp_pval.second)) - g.GetY()[g.GetN() - 1];
                    if (i >= 0) { g.SetPointEYhigh(g.GetN() - 1, diff); }
                    else { g.SetPointEYlow(g.GetN() - 1, -diff); }
                }
            } catch(UnavailableException& e) {
                //std::cout << "Skipping expected point @ mu = " << x << std::endl;
                // skip this point because fit unavailable
                // TODO: 1sigma and 2sigma band handling when no 0sigma available?
            }
        }
        //gSigma_mu.SetPoint(gSigma_mu.GetN(), x, *sigma_mu);
        delete sigma_mu;
    }


    s.setReadOnly(false);

    TCanvas* c = new TCanvas("PValuePlot","Asymptotic PValue Scan");
    TMultiGraph* mg = new TMultiGraph; mg->SetName("graphs");

    if (gExp[2].GetN()) {
        mg->Add( (TGraph*)gExp[2].Clone("expected2"),"AE3");
    }
    if (gExp[1].GetN()) {
        mg->Add( (TGraph*)gExp[1].Clone("expected1"),TString("E3") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }
    if (gExp[0].GetN()) {
        mg->Add( (TGraph*)gExp[0].Clone("expected"),TString("L") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }
    if (gObs.GetN()) {
        mg->Add( (TGraph*)gObs.Clone("observed"),TString("LP") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }
    if (gObs_null.GetN()) {
        mg->Add( (TGraph*)gObs_null.Clone("observed_null"),TString("LP") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }
    if (gObs_alt.GetN()) {
        mg->Add( (TGraph*)gObs_alt.Clone("observed_alt"),TString("LP") + (c->GetListOfPrimitives()->GetEntries() ? "" : "A"));
    }

    mg->SetTitle(Form(";%s;%s",s.first_poi()->GetTitle(), s.pllModifier() == FitManager::TwoSided ? "#tilde{t}_{#mu} p value" : "#tilde{q}_{#mu} p value"));
    mg->Draw("A E3");

    if(gPad) gPad->RedrawAxis();

    return c;


}

