//
// Created by Will Buttinger on 2019-10-13.
//

#include "RooFit.h"

#include "Rtypes.h"
#include "Riostream.h"
#include "TEnv.h"

#include "FitManagerVersion.h"

#include <iostream>

using namespace std;

Int_t doFitManagerBanner();

static Int_t dummyFMB = doFitManagerBanner() ;

Int_t doFitManagerBanner()

{
#ifndef __TROOFIT_NOBANNER
    cout << "\033[1mFitManager   -- Development ongoing\033[0m " << endl
         << "                Fit Manager and tools for RooWorkspaces : http://gitlab.cern.ch/will/FitManager" << endl << "                version: " << GIT_COMMIT_HASH <<
         endl;
#endif
    (void) dummyFMB;
    return 0 ;
}
