#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#include "FitManager/FitManager.h"
#pragma link C++ class FitManager+;
#pragma link C++ class FitManager::FitConfig+;
#pragma link C++ class FitManager::FitResult+;
#pragma link C++ class FitManager::GeneratedData+;

#endif