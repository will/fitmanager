//
// Created by Will Buttinger on 24/08/2020.
//

#ifndef FITMANAGER_H
#define FITMANAGER_H

#include "TObject.h"



#define protected public
#define private public
//#include "RooStats/AsymptoticCalculator.h"
#include "RooWorkspace.h"
#undef private
#include "RooFitResult.h"
#undef protected

#include "RooRealVar.h"
#include "RooAbsPdf.h"
#include "RooAbsData.h"

#include "Fit/FitConfig.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"

#include <string>
#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <memory>
#include <exception>
#include <functional>

class FitManager {

public:
    struct FitConfig {
        std::string uuid;
        ROOT::Fit::FitConfig fitConfig;
        RooLinkedList nllOpts;
        std::string pdfName;
        std::string rootVersion;
        const ROOT::Math::MinimizerOptions& minimizerOptions() const { return const_cast<ROOT::Fit::FitConfig&>(fitConfig).MinimizerOptions(); } // needed because MinimizerOptions method not const

        void prepare(FitManager*) { }
        std::string _uuid() const { return uuid; }
        void _copy(FitConfig*) { }
    };
    struct FitResult {
        std::string dataName;
        const RooFitResult* fitResult = nullptr; //->
        std::string fitConfig_uuid;
        FitManager* helper = nullptr; //!
        FitConfig* fitConfig() {
            return helper->fitConfigs().find([&](const FitConfig& fc) { return fc.uuid == fitConfig_uuid; });
        }
        void Print() const {
            std::cout << "fitConfig_uuid: " << fitConfig_uuid << std::endl;
            std::cout << "dataName: " << dataName << std::endl;
            if (fitResult) fitResult->Print("v");
        }
        void prepare(FitManager* s) { helper = s; }
        std::string _uuid() const { return (!fitResult) ? "" : fitResult->GetName(); }
        void _copy(FitResult* other) { fitResult = (RooFitResult*)other->fitResult->Clone(); }
    };
    struct GeneratedData {
        TUUID fitResult_uuid; // the ID of the conditional fit that generated this data
        RooAbsData* data = nullptr; //!
        std::string dataName;
        RooArgSet* gobs = nullptr;
        void prepare(FitManager* s) {
            data = s->ws()->data(dataName.c_str());
        }
        std::string _uuid() const { return (data) ? data->GetName() : dataName; }
        void _copy(GeneratedData* other) { gobs = (RooArgSet*)other->gobs->Clone(); }
    };


    FitManager(RooWorkspace* w = nullptr, const char* poi = "mu");
    void writeToFile(const char* filename, const char* wsFileName=nullptr, bool newOnly=false);

    void ScanAndCalculate(double low, double high, double spacing, std::function<void(FitManager&)> calc, int nprocs=1);



    // values of the first_poi in fits where the poi was held constant (aka a conditional fit)
    std::set<double> conditionalValues();

    void setReadOnly(bool in) { m_locked = in; }

    typedef bool (*PLLModifier)(double mu, double mu_hat);


    void saveFit(const RooAbsData& data, const RooFitResult& result);




    // data member accessors
    RooWorkspace* ws() { return m_ws; }
    RooAbsPdf* pdf() { return m_pdf; } // the currently selected top-level pdf
    std::pair<RooAbsData*,const RooArgSet*> data() { return m_data; } // the currently selected observed data
    RooAbsReal* nll();
    RooArgSet* obs();
    RooArgSet* globs();
    RooArgSet& poi() { return m_poi; }
    RooRealVar* first_poi() { if (m_poi.empty()) return nullptr; return dynamic_cast<RooRealVar*>(m_poi.first()); }
    PLLModifier pllModifier() { return m_pll_modifier; }
    const RooLinkedList& nllOpts() { return m_fitConfig.nllOpts; }
    const FitConfig& fitConfig() { return m_fitConfig; }

    void setFitConfig(ROOT::Fit::FitConfig& config) {
        m_fitConfig.fitConfig = config; m_fitConfig.uuid = ""; // TODO: search for matching fit config instead of going new
    }

    void setGlobs(const RooArgSet* globs);

    // data member setters
    void setPdf(RooAbsPdf* _pdf);
    std::pair<RooAbsData*,const RooArgSet*> setData(std::pair<RooAbsData*,const RooArgSet*> _data) { return setData(_data.first,_data.second); }
    std::pair<RooAbsData*,const RooArgSet*> setData(RooAbsData* _data, const RooArgSet* _globs = nullptr);
    void setPOI(const char* poi);
    void setPllModifier( PLLModifier _compatibilityFunction ) { m_pll_modifier = _compatibilityFunction; }

    /* Expensive Computations */
    RooFitResult* fit();
    std::pair<RooAbsData*, RooArgSet*> generateExpected();

    double expected_pll(double nSigma = 0,
                        double expected_mu = 0,
                        double** _sigma_mu = nullptr) {
        return pll(nullptr,std::make_shared<double>(nSigma).get(),_sigma_mu,expected_mu);
    }

    /* Calculations */
    double pll(
            RooFitResult** _unconditionalFit = nullptr, /* set to 'save/reuse' unconditional fit */
            double* nSigma = nullptr, /* set to return expected rather than observed pll */
            double** _sigma_mu = nullptr, /* set to 'save' sigma_mu value (or reuse) - only relevant for expected pll */
            double alt_val = 0 /* used to estimate sigma_mu from an asimov dataset with mu=alt_val. mu=0 is usual case. */
    );
    double sigma_mu(double asimovValue); // estimate of variance of mu_hat

    // return null and alt p-values from asymptotic formulae
    std::pair<double,double> asymptoticPValue(

            RooFitResult** _unconditionalFit = nullptr,
            double* nSigma = nullptr,
            double** _sigma_mu = nullptr,
            double alt_val = 0
    );



    //some basic compatibility functions
    static bool TwoSided(double mu, double mu_hat) { return mu==mu_hat; }
    static bool OneSidedPositive(double mu, double mu_hat) { return mu_hat>=mu; }
    static bool OneSidedNegative(double mu, double mu_hat) { return mu_hat<=mu; }
    static bool OneSidedAbsolute(double mu, double mu_hat) { return fabs(mu_hat) >= fabs(mu); }
    static bool Uncapped(double mu,double mu_hat) { return OneSidedNegative(mu,mu_hat); } //this function is treated specially inside the methods that use it



    //this is just like RooAbsCollection::setAttribAll but it also dirties all the elements.
    static void setAttribAll(const RooAbsCollection& coll, const char* name, Bool_t value);

    // returns Int_[k->inf] { p( t_mu | mu = mu' ) dt_mu } where t_mu is the test statistic defined by compatibilityFunction
    // mu' (mu_prime) is the true value of mu
    // mu is the value of mu being tested (part of the t_mu definition)
    static Double_t PValue(double k, double mu, double mu_prime, double sigma_mu, double mu_low, double mu_high, PLLModifier _compatibilityFunction);
    static Double_t nullPValue(double k, RooRealVar* mu_test, double sigma_mu, PLLModifier _compatibilityFunction ) {
        return PValue(k,mu_test->getVal(),mu_test->getVal(),sigma_mu,mu_test->getMin("physical"),mu_test->getMax("physical"),_compatibilityFunction);
    }
    static Double_t altPValue(double k, RooRealVar* mu_test, double alt_mu, double sigma_mu,PLLModifier _compatibilityFunction) {
        return PValue(k,mu_test->getVal(),alt_mu,sigma_mu,mu_test->getMin("physical"),mu_test->getMax("physical"),_compatibilityFunction);
    }

    static TObject* m_msgObj;
    static TObject& msg() { if(m_msgObj==0) m_msgObj=new TObject; return *m_msgObj; }


    static Double_t Phi_m(double mu, double mu_prime, double a, double sigma, PLLModifier _compatibilityFunction);


    RooWorkspace* m_ws = nullptr;
    RooAbsPdf* m_pdf = nullptr;
    std::pair<RooAbsData*, const RooArgSet*> m_data; // first is obs, second is globs
    RooArgSet* m_obs = nullptr; // filled when call setData

    RooAbsReal* m_nll = nullptr;

    RooArgSet m_poi;

    PLLModifier m_pll_modifier = TwoSided; // two-sided is no modification
    FitConfig m_fitConfig;



    template <typename T> struct StoredCollection {

        void load(TDirectory* d) {
            TString treeName = m_name +"_"+ s->ws()->uuid().AsString();
            auto t = dynamic_cast<TTree*>( d->Get( treeName ) );
            if (!t) return;
            T* obj = nullptr;
            auto nentries = t->GetEntriesFast();
            t->SetBranchAddress(".",&obj);
            // can 'skip' checking the tree by temporarily hiding it
            auto tmp = m_tree; m_tree = nullptr;
            size_t count=0;
            for(size_t i = 0; i < nentries; i++) {
                t->GetEntry(i);
                if (find(obj->_uuid())) continue;
                auto new_obj = new T(*obj); new_obj->_copy(obj);
                m_mem.push_back( new_obj );
                count++;
            }
            Info("load","Loaded %zu %s from %s",count,m_name.Data(),d->GetName());
            m_tree = tmp;
        }

        size_t size() const {
            return m_mem.size() + ((m_tree) ? m_tree->GetEntriesFast() : 0);
        }

        T* at(size_t idx) const {
            size_t on_disk = (m_tree) ? m_tree->GetEntriesFast() : 0;
            if (on_disk > idx) {
                T *f = nullptr;
                m_tree->SetBranchAddress(".", &f);
                m_tree->GetEntry(idx);
                return f;
            } else if(m_mem.size() + on_disk > idx) {
                return m_mem.at(idx - on_disk);
            }
            return nullptr;
        }

        T findCopy(const std::function<bool(const T&)>& selector) const {
            T* o = find(selector);
            if(o) { return *o; }
            return T();
        }

        T* find(const std::string& uuid) const {
            return find([&](const T& o) { return o._uuid().find(uuid)==0; });
        }

        T* find(const std::function<bool(const T&)>& selector) const {
            for(auto& f : m_mem) {
                if (selector(*f)) { return f; }
            }

            //check on-disk objects, if any
            if (m_tree) {
                T *f = nullptr;
                m_tree->SetBranchAddress(".", &f);
                auto nEntries = m_tree->GetEntriesFast();
                for (size_t i = 0; i < nEntries; i++) {
                    m_tree->GetEntry(i);
                    if (!f) continue;
                    f->prepare(s); // can configure any 'linked' objects etc
                    if (selector(*f)) {
                        return f; // return a 'clone' instead?
                    }
                }
            }
            return nullptr;
        }
        std::vector<T*> m_mem; // objects in memory
        TTree* m_tree = nullptr; // objects on disk
        TString m_name;
        FitManager* s = nullptr;

        void init(const std::string& name, FitManager* _s) {
            m_name = name; s = _s;
            TFile *f;
            TString treeName = m_name +"_"+ s->ws()->uuid().AsString();
            m_tree = dynamic_cast<TTree*>( gDirectory->Get( treeName ) );
            if (!m_tree) {
                // check all open files
                R__LOCKGUARD(gROOTMutex);
                TIter next(gROOT->GetListOfFiles());
                while ((f = (TFile*)next())) {
                    m_tree = dynamic_cast<TTree*>(f->Get(treeName));
                    if (m_tree) break;
                }
            }
            if (m_tree) {
                Info("FitManager::FitManager","Found %lld %s", m_tree->GetEntriesFast(), m_name.Data());
            }
        }

        void save(bool newOnly = false) {
            if ((!m_tree||newOnly) && m_mem.empty()) return; // nothing to save
            T* obj = nullptr;
            TTree* old_tree = m_tree;
            if(!m_tree || newOnly) {
                m_tree = new TTree(m_name + "_" + s->ws()->uuid().AsString(), m_name);
                m_tree->Branch(".",&obj);
            } else {
                m_tree->SetBranchAddress(".",&obj);
                m_tree = m_tree->CloneTree();
            }
            for(auto& o : m_mem) {
                obj = o;
                m_tree->Fill();
            }
            m_tree->Write();
            m_tree->ResetBranchAddresses();
            Info("writeToFile","Wrote %lld %s",m_tree->GetEntriesFast(),m_name.Data());
            m_tree = old_tree;
        }

    };

    StoredCollection<FitConfig> m_fitConfigs;
    StoredCollection<GeneratedData> m_generated;
    StoredCollection<FitResult> m_fitResults;

    const StoredCollection<FitResult>& fitResults() { return m_fitResults; }
    const StoredCollection<FitConfig>& fitConfigs() { return m_fitConfigs; }
    const StoredCollection<GeneratedData>& generatedData() { return m_generated; }

    // utility class to snapshot a collection and have it auto-restore them on destruction
    class AutoRestorer {
    public:
        explicit AutoRestorer(const RooAbsCollection& coll) :  m_snap(coll.snapshot()) { m_coll.add(coll); }
        ~AutoRestorer() {
            static_cast<RooAbsCollection&>(m_coll) = *m_snap; delete m_snap; }
        RooArgSet* values() { return dynamic_cast<RooArgSet*>(m_snap); }
    private:
        RooArgSet m_coll;
        RooAbsCollection* m_snap;
    };

private:
    bool m_locked = false;


public:
    // Theses methods will be relocated to Tools
    static TCanvas* DrawAsymptoticPValue(FitManager& s, bool useCLs=true, double alt_val = 0);
    static TCanvas* DrawPLL(FitManager& s);
    static double SearchAsymptoticPValue(FitManager& s, double low, double high, bool useCLs=true, double* nSigma=nullptr, double alt_val=0, double alpha=0.05);
    static void ScanAsymptoticPValue(FitManager& s, double low, double high, double spacing, std::vector<double>&& sigmas={}, double alt_val = 0);
    static std::pair<double,double> GetLimit(FitManager& s, bool useCLs=true, double* nSigma=nullptr, double alt_val=0, double target=0.05);


};


class UnavailableException : public std::exception {
    const char * what () const throw () {
        return "Unavailable";
    }
};

class FitUnavailableException : public UnavailableException {
    const char * what () const throw () {
        return "Fit Unavailable";
    }
};

class DataUnavailableException : public UnavailableException {
    const char * what () const throw () {
        return "Data Unavailable";
    }
};


#endif //OBJSTORE_ASYMPTOTICLIMITS_H
