



file(GLOB HEADERS FitManager/FitManager.h)

ROOT_GENERATE_DICTIONARY(G__FitManager ${HEADERS} LINKDEF FitManager/LinkDef.h)

file(GLOB SOURCES src/*.cpp)

add_library(FitManager SHARED ${SOURCES} G__FitManager )
target_link_libraries(FitManager ${ROOT_LIBRARIES})

target_sources(FitManager PRIVATE ${CMAKE_BINARY_DIR}/versioning/FitManagerVersion.h)
set_source_files_properties(${CMAKE_BINARY_DIR}/versioning/FitManagerVersion.h PROPERTIES GENERATED TRUE)
target_include_directories(FitManager PRIVATE ${CMAKE_BINARY_DIR}/versioning)
target_include_directories(FitManager PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
add_dependencies(FitManager fitmanager_version)


enable_testing()
include(GoogleTest)
add_executable(TestFitManager test/TestFitManager.cpp)
target_link_libraries(TestFitManager FitManager gtest gtest_main)

install( DIRECTORY FitManager DESTINATION include FILES_MATCHING
        COMPONENT headers
        PATTERN "*.h"
        )

install( FILES ${CMAKE_CURRENT_BINARY_DIR}/libFitManager.rootmap
        ${CMAKE_CURRENT_BINARY_DIR}/libFitManager_rdict.pcm
        DESTINATION lib
        COMPONENT libraries)

install(TARGETS FitManager
        LIBRARY DESTINATION lib
        COMPONENT libraries)